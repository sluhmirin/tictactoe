# README #

Simple TicTacToe game with some awesome features. 

It uses AndroidAnnotations and mostly canvas.

Version: 0.42

Feel free to contribute.

--------------------
Copyright © 2013 Sergejs Luhmirins <sergejs.luhmirins@gmail.com>

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See http://www.wtfpl.net/ for more details.