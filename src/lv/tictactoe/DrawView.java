package lv.tictactoe;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import lv.tictactoe.logic.FieldValues;
import lv.tictactoe.logic.GameObject;
import lv.tictactoe.utils.Consts;
import lv.tictactoe.utils.Utils;

/**
 * Basic view for drawing tic tac toe field.
 */

public class DrawView extends View {

    private final Point mSelected = new Point(-1, -1);
    private final Paint mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    //View size
    private int mWidth = 0;
    private int mHeight = 0;
    //Size of one cell
    private int mCellX = 0;
    private int mCellY = 0;
    private boolean freezeCanvas = false;
    private GameObject mGame;
    private Canvas mCanvas;
    private TurnView mTurnView = null;

    public DrawView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        mGame = new GameObject();
    }

    public void setupGameOptions(int pl, int sz, int tw){

        if (sz == 0) {
            mGame = new GameObject();
        } else if (pl == 0) {
            mGame = new GameObject(sz);
        } else if (tw == 0) {
            mGame = new GameObject(pl,sz);
        } else {
            mGame = new GameObject(pl,sz,tw);
        }

    }

    public void setTurnLabel(TurnView label) {
        this.mTurnView = label;
        mTurnView.setParent(this);
    }

    public void setTurn() {
        if (mTurnView != null) {
            mTurnView.setValue(mGame.getPlayerTurn());
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        mCanvas = canvas;

        mWidth = this.getWidth();
        mHeight = this.getHeight();

        mCellX = (mWidth - Consts.OFFSET_COUNT * Consts.GRID_OFFSET) / mGame.getSize();
        mCellY = (mHeight - Consts.OFFSET_COUNT * Consts.GRID_OFFSET) / mGame.getSize();
        drawGrid();
        drawMatrix();

        setTurn();

        if (mGame.getmWinner() != FieldValues.NONE) {
            drawWinner();
            freezeCanvas = true;
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        if (!freezeCanvas) {

            //coordinates of touched square
            int x = (int) (event.getX() - Consts.GRID_OFFSET) / mCellX;
            int y = (int) (event.getY() - Consts.GRID_OFFSET) / mCellY;

            if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
                mSelected.set(x, y);
            }
            if (event.getAction() == MotionEvent.ACTION_UP) {
                mGame.moveWasMade(x, y);
                mSelected.set(-1, -1);
            }

            redraw();

        }
        return true;
    }

    /**
     * Drawing grid on field.
     */
    private void drawGrid() {

        mCanvas.drawColor(Consts.BG_COLOR);
        drawSelected();

        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Consts.GRID_COLOR);
        mPaint.setStrokeWidth(Consts.GRID_STROKE);

        for (int i = 1; i < mGame.getSize(); i++) {
            //vertical
            mCanvas.drawLine(mCellX * i + Consts.GRID_OFFSET, Consts.GRID_OFFSET, mCellX * i + Consts.GRID_OFFSET, mHeight - Consts.GRID_OFFSET, mPaint);
            //horizontal
            mCanvas.drawLine(Consts.GRID_OFFSET, mCellY * i + Consts.GRID_OFFSET, mWidth - Consts.GRID_OFFSET, mCellY * i + Consts.GRID_OFFSET, mPaint);
        }

    }

    /**
     * Filling grid with figures.
     */
    private void drawMatrix() {

        FieldValues[][] matrix = mGame.getMatrix();

        for (int y = 0; y < mGame.getSize(); y++) {

            for (int x = 0; x < mGame.getSize(); x++) {

                if (matrix[y][x] != FieldValues.EMPTY) {
                    drawFigure(x, y, matrix[y][x]);
                }
            }
        }

    }

    /**
     * Drawing concrete figure in grid.
     *
     * @param y   - line
     * @param x   - column
     * @param val - figure to draw
     */
    private void drawFigure(int y, int x, FieldValues val) {
        RectF rect = getCellRect(x, y);
        Utils.drawFigure(mCanvas,val,rect);
    }

    private void drawSelected() {
        if (mSelected.x != -1 && mSelected.y != -1) {
            RectF rect = getCellRect(mSelected.x, mSelected.y);
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setColor(Consts.SELECTED_COLOR);
            mCanvas.drawRect(rect, mPaint);
        }
    }

    private void drawWinner() {

        mPaint.setColor(Consts.TEXT_COLOR);
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setTextAlign(Paint.Align.CENTER);
        mPaint.setStrokeWidth(Consts.TEXT_STROKE);
        mPaint.setTextSize(Consts.TEXT_SIZE);

        String winnerString = getResources().getString(R.string.winner);
        int textX = mWidth - (int) mPaint.measureText(winnerString);

        mCanvas.drawText(winnerString, (float)(mHeight / 2), (float)(mWidth / 2), mPaint);

    }

    public void redraw() {
        this.postInvalidate();
    }

    public void reset() {
        mGame.reset();
        freezeCanvas = false;
        this.postInvalidate();
    }

    private RectF getCellRect(int x, int y) {
        //coordinates in which figure will be drawn.
        int squareX0 = Consts.GRID_OFFSET + x * mCellX + Consts.CELL_OFFSET;
        int squareY0 = Consts.GRID_OFFSET + y * mCellY + Consts.CELL_OFFSET;
        int squareXE = Consts.GRID_OFFSET + x * mCellX + mCellX - Consts.CELL_OFFSET;
        int squareYE = Consts.GRID_OFFSET + y * mCellY + mCellY - Consts.CELL_OFFSET;
        return new RectF(squareX0, squareY0, squareXE, squareYE);
    }

}
