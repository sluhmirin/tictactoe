package lv.tictactoe;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.googlecode.androidannotations.annotations.*;
import lv.tictactoe.utils.Consts;

@EActivity(R.layout.game)
public class GameActivity extends Activity {

    @ViewById(R.id.field)
    DrawView mDrawView;

    @ViewById(R.id.turnView)
    TurnView tvTurn;

    @Extra(Consts.EXTRA_PLAYER_COUNT)
    int playerCount = Consts.MIN_PLAYER_COUNT;

    @Extra(Consts.EXTRA_FIELD_SIZE)
    int fieldSize = Consts.DEF_SIZE;

    @Extra(Consts.EXTRA_TO_WIN)
    int toWin = Consts.TO_WIN_DEF;

    @AfterViews
    public void initAll(){

        mDrawView.setTurnLabel(tvTurn);
        mDrawView.setupGameOptions(playerCount,fieldSize, toWin);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int size = metrics.widthPixels;
        mDrawView.getLayoutParams().height = size;
        mDrawView.getLayoutParams().width = size;

    }

}
