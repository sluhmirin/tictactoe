package lv.tictactoe;

import android.app.Activity;
import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;
import lv.tictactoe.utils.Consts;

/**
 * Starting activity.
 */
@EActivity(R.layout.main)
public class MainActivity extends Activity {


    @Click(R.id.btnClassic)
    public void btnTwoClick() {
        startGame(2,3,3);
    }

    @Click(R.id.btnTwoLarge)
    public void btnTwoLargeClick() {
        startGame(2,10,5);
    }

    @Click(R.id.btnThree)
    public void btnThreeClick() {
        startGame(3, 10,4);
    }

    void startGame(int players, int size, int win) {
        Intent intent = new Intent(this, GameActivity_.class);
        intent.putExtra(Consts.EXTRA_PLAYER_COUNT, players);
        intent.putExtra(Consts.EXTRA_FIELD_SIZE, size);
        intent.putExtra(Consts.EXTRA_TO_WIN, win);
        startActivity(intent);
    }
}