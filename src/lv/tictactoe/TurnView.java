package lv.tictactoe;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import lv.tictactoe.logic.FieldValues;
import lv.tictactoe.utils.Consts;
import lv.tictactoe.utils.Utils;

/**
 * Additional view for helpful stuff.
 */
public class TurnView extends View {

    private FieldValues mValue = FieldValues.NONE;
    private DrawView mParent = null;
    private Canvas mCanvas;


    public TurnView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setValue(final FieldValues mValue) {
        this.mValue = mValue;
        this.postInvalidate();
    }

    public void setParent(final DrawView mParent) {
        this.mParent = mParent;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mCanvas = canvas;

        mCanvas.drawColor(Consts.BG_TURN_COLOR);


        RectF turnRect = new RectF(50,50,250,250);

        drawNextTurnLabel(turnRect);


    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        int x = (int) event.getX();
        int y = (int) event.getY();

        if (event.getAction() == MotionEvent.ACTION_DOWN ) {
            mParent.reset();
        }
        return true;
    }

    private void drawNextTurnLabel(RectF rect){

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);

        paint.setColor(Consts.BG_COLOR);
        mCanvas.drawRect(rect,paint);

        paint.setStrokeWidth(Consts.TEXT_STROKE);
        paint.setColor(Consts.TEXT_COLOR);
        paint.setTextSize(Consts.TEXT_TURN_SIZE);


        float textX = rect.centerX() - paint.measureText(getResources().getString(R.string.tv_turn))/2;
        float textY = (float) rect.top+ Consts.TURN_VIEW_OFFSET + Consts.TEXT_TURN_SIZE;

        mCanvas.drawText(getResources().getString(R.string.tv_turn),textX,textY,paint);

        float left = rect.left + Consts.TURN_VIEW_OFFSET;
        float right = rect.right - Consts.TURN_VIEW_OFFSET;
        float top = textY + Consts.TURN_VIEW_OFFSET *2;
        float bottom = rect.bottom - Consts.TURN_VIEW_OFFSET;

        RectF rect2 = new RectF(left, top, right, bottom);

        Utils.drawFigure(mCanvas,mValue,rect2);
    }

}
