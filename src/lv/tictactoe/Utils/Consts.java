package lv.tictactoe.utils;

import android.graphics.Color;
import lv.tictactoe.R;

/**
 * Created with IntelliJ IDEA.
 * User: Sergejs.Luhmirins
 * Date: 4/17/13
 * Time: 5:27 PM
 * To change this template use File | Settings | File Templates.
 */
public class Consts {

    //Game logic constants
    public static final int MOVE_TIC = 0;
    public static final int MOVE_TAC = 1;
    public static final int MOVE_TOE = 2;
    public static final int MIN_PLAYER_COUNT = 2;

    //Intent constants
    public static final String EXTRA_PLAYER_COUNT = "player_count";
    public static final String EXTRA_FIELD_SIZE = "field_size";

    //Log constants
    @SuppressWarnings("UnusedDeclaration")
    public static final String TAG = "myLog";

    //Matrix constants
    public static final int DEF_SIZE = 3;
    public static final int MAX_SIZE = 15;
    public static final int BORDER_SIZE = 5;
    public static final int TO_WIN_DEF = 3;
    public static final int TO_WIN_AVERAGE = 4;
    public static final int TO_WIN_LARGE = 5;

    //Colors
    public static final int BG_COLOR = Color.parseColor("#ffdcdcdc");
    public static final int SELECTED_COLOR = Color.parseColor("#ffbbbbbb");
    public static final int GRID_COLOR = Color.parseColor("#ff000000");
    public static final int TEXT_COLOR = Color.parseColor("#ff000000");
    public static final int TIC_COLOR = Color.parseColor("#ff2828c8");
    public static final int TAC_COLOR = Color.parseColor("#ffc82828");
    public static final int TOE_COLOR = Color.parseColor("#ff28c828");

    //DrawView constants
    public static final int GRID_OFFSET = 5;
    public static final int CELL_OFFSET = 5;
    public static final int OFFSET_COUNT = 2;
    public static final int GRID_STROKE = 2;
    public static final int TEXT_STROKE = 3;
    public static final int FIGURE_STROKE = 2;
    public static final int TEXT_SIZE = 50;

}
