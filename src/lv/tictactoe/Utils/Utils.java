package lv.tictactoe.utils;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import lv.tictactoe.logic.FieldValues;

/**
 * Useful utilities.
 */
public class Utils {

    public static void drawFigure(Canvas canvas, FieldValues val, RectF rect){

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        paint.setStrokeWidth(Consts.FIGURE_STROKE);
        paint.setStyle(Paint.Style.STROKE);

        if (val == FieldValues.TIC) {
            paint.setColor(Consts.TIC_COLOR);
            canvas.drawLine(rect.left, rect.top, rect.right, rect.bottom, paint);
            canvas.drawLine(rect.left, rect.bottom, rect.right, rect.top, paint);
        } else if (val == FieldValues.TAC) {
            paint.setColor(Consts.TAC_COLOR);
            canvas.drawOval(rect, paint);
        } else {
            paint.setColor(Consts.TOE_COLOR);
            canvas.drawLine(rect.left, rect.bottom, rect.right, rect.bottom, paint);
            canvas.drawLine(rect.left, rect.bottom, rect.centerX(), rect.top, paint);
            canvas.drawLine(rect.right, rect.bottom, rect.centerX(), rect.top, paint);
        }
    }

}
