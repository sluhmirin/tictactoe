package lv.tictactoe.logic;

/**
 * Enum of field values.
 */
public enum FieldValues {
    EMPTY,
    TIC,
    TAC,
    TOE,
    NONE
}
