package lv.tictactoe.logic;


import lv.tictactoe.utils.Consts;

/**
 * Object for game logic.
 */
public class GameObject {

    private Matrix matrix;

    private FieldValues mWinner = FieldValues.NONE;

    private int move = Consts.MOVE_TIC;
    private int playerCount = Consts.MIN_PLAYER_COUNT;
    private int fieldSize = Consts.DEF_SIZE;

    public GameObject() {
        this(Consts.MIN_PLAYER_COUNT, Consts.DEF_SIZE, Consts.TO_WIN_DEF );
    }

    public GameObject(int field) {
        this(Consts.MIN_PLAYER_COUNT, field, Consts.TO_WIN_DEF );
    }

    public GameObject(int players, int field) {
        this(players, field, Consts.TO_WIN_DEF );
    }

    public GameObject(int players, int field, int win) {
        this.fieldSize = field;
        this.playerCount = players;
        matrix = new Matrix(fieldSize, win);
    }

    public FieldValues[][] getMatrix() {
        return matrix.getMatrix();
    }

    public FieldValues getmWinner() {
        return mWinner;
    }

    public void moveWasMade(int x, int y) {

        boolean result = matrix.setCellValue(x, y, getPlayerTurn());
        if (result) {
            if (matrix.checkWin(x, y)) {
                mWinner = matrix.getCellValue(x, y);
            }
            move = (move + 1) % playerCount;
        } else {
            mWinner = FieldValues.NONE;
        }
    }

    public int getSize() {
        return matrix.getSize();
    }

    public void reset() {
        matrix.resetMatrix();
        mWinner = FieldValues.NONE;
        move = Consts.MOVE_TIC;
    }

    public FieldValues getPlayerTurn() {
        switch (move) {
            case Consts.MOVE_TIC:
                return FieldValues.TIC;
            case Consts.MOVE_TAC:
                return FieldValues.TAC;
            case Consts.MOVE_TOE:
                return FieldValues.TOE;
            default:
                return FieldValues.NONE;
        }
    }

}
