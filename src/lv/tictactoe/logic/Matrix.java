package lv.tictactoe.logic;

import lv.tictactoe.utils.Consts;

/**
 * Object of game field.
 */
@SuppressWarnings("ConstantConditions")
class Matrix {

    private FieldValues[][] mMatrix;

    private int mSize;
    private int mToWin = Consts.TO_WIN_DEF;

    public Matrix(int size, int win) {

        this.mSize = validateSize(size);
        this.mToWin = validateWin(win);

        resetMatrix();
    }

    private int validateWin(final int win){
        if (win <= Consts.TO_WIN_DEF) return Consts.TO_WIN_DEF;
        if (win >= Consts.TO_WIN_LARGE) return Consts.TO_WIN_LARGE;
        return Consts.TO_WIN_AVERAGE;
    }

    private int validateSize(final int size){
        if (size < Consts.DEF_SIZE) return Consts.DEF_SIZE;
        if (size > Consts.MAX_SIZE) return Consts.MAX_SIZE;
        return size;
    }

    /**
     * @return all mMatrix
     */
    public FieldValues[][] getMatrix(){
        return mMatrix;
    }

    public void resetMatrix(){

        mMatrix = new FieldValues[this.mSize][this.mSize];
        for (int y = 0; y < mSize; y++){
            for(int x = 0; x < mSize; x++){
                mMatrix[x][y] = FieldValues.EMPTY;

            }
        }
    }

    /**
     * Method to get cell value.
     *
     * @param x - column
     * @param y - row
     * @return value of concrete cell
     */
    public FieldValues getCellValue(int x, int y){
         if(isInBounds(x,y)){
             return mMatrix[x][y];
         }
         return FieldValues.NONE;
    }

    /**
     * Method to set cell value.
     *
     * @param x - column
     * @param y - row
     * @param value - value to be set
     * @return true if values is set, false otherwise.
     */
    public boolean setCellValue(int x, int y, FieldValues value){
         if(isInBounds(x,y)){
             if (mMatrix[x][y] == FieldValues.EMPTY){
                 mMatrix[x][y] = value;
                 return true;
             }
         }
        return false;
    }

    public int getSize() {
        return mSize;
    }

    private boolean isInBounds(int x, int y){
        return (x < mSize && x >=0 && y < mSize && y >=0 );
    }

    @SuppressWarnings("ConstantConditions")
    public boolean checkWin(int x, int y){
        //vertical
        int inRow = 1;
        int temp = x-1;
        while(temp >= 0 && mMatrix[temp][y] == mMatrix[x][y]){
            inRow++;
            temp--;
        }
        temp = x+1;
        while(temp < mSize && mMatrix[temp][y] == mMatrix[x][y]){
            inRow++;
            temp++;
        }
        if(inRow >= mToWin){
            return true;
        }
        //horizontal
        inRow = 1;
        temp = y-1;
        while(temp >= 0 && mMatrix[x][temp] == mMatrix[x][y]){
            inRow++;
            temp--;
        }
        temp = y+1;
        while(temp < mSize && mMatrix[x][temp] == mMatrix[x][y]){
            inRow++;
            temp++;
        }
        if(inRow >= mToWin){
            return true;
        }
        //diagonally up -> down
        inRow = 1;
        int tempY = y - 1;
        int tempX = x - 1;
        while(tempY < mSize && tempX < mSize && tempY >= 0 && tempX >= 0 && mMatrix[tempX][tempY] == mMatrix[x][y]){
            inRow++;
            tempX--;
            tempY--;
        }
        tempY = y + 1;
        tempX = x + 1;
        while(tempY < mSize && tempX < mSize && tempY >= 0 && tempX >= 0 && mMatrix[tempX][tempY] == mMatrix[x][y]){
            inRow++;
            tempX++;
            tempY++;
        }
        if(inRow >= mToWin){
            return true;
        }
        //diagonally down -> up
        inRow = 1;
        tempY = y - 1;
        tempX = x + 1;
        while(tempY < mSize && tempX < mSize && tempY >= 0 && tempX >= 0 && mMatrix[tempX][tempY] == mMatrix[x][y]){
            inRow++;
            tempX++;
            tempY--;
        }
        tempY = y + 1;
        tempX = x - 1;
        while(tempY < mSize && tempX < mSize && tempY >= 0 && tempX >= 0 && mMatrix[tempX][tempY] == mMatrix[x][y]){
            inRow++;
            tempX--;
            tempY++;
        }
        return inRow >= mToWin;
    }

}
